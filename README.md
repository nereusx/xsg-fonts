# xsg-fonts

Target: Clear and easy to read bitmap unicode fonts. No art, just clear as possible and a bit condensed.

## Charsets:

* Latin1
* Latin2
* Greek
* Cyrilic (I hope, fix and send)
* Drawing box
* Several more characters mostly for icons and polybar

## SIL Open Font License (OFL)
https://scripts.sil.org/OFL

## Install

### Simple

```
git clone https://github.com/nereusx/xsg-fonts
cd xsg-fonts/pcf
sudo ./install.sh
```

### Manual

As root:

1. copy `*.pcf.gz` files to Xorg's bitmap (`misc`) fonts directory.
2. run `fc-cache -rvf` or restart the X.

## Edit fonts

1. Use **fontforge** to edit the font (`sfd` files)
2. From **fontforge** export to bitmap `bdf` file.
3. Convert to `pcf` by using **bdftopcf** Xorg's utility

## Notes:

* xsg-fixed names:

	xsg-fixedYYX:
	YY = height in pixels, X  = width in pixels

	(Standard VGA Fonts = 9x16)
	
	The R in the end of the file means "Regular" (non-bold)

## Links
* [FontForge](http://fontforge.github.io/en-US/) font editor.
* [Unicode VGA font](http://www.inp.nsk.su/~bolkhov/files/fonts/univga/).
* [THE OLDSCHOOL PC FONTS](https://int10h.org/oldschool-pc-fonts/fontlist/).
* Convert bitmap fonts to truetype with [Bits'N'Picas](https://github.com/kreativekorp/bitsnpicas).
