#!/bin/sh

for f in *.bdf; do
	base=$(basename -s .bdf "$f")
	pcf=$(echo "$base" | sed 's/-[0-9][0-9]//g')
	echo "$f -> ${pcf}.pcf"
	bdftopcf "$f" -o pcf/${pcf}.pcf
	gzip -f pcf/${pcf}.pcf
done
