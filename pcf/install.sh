#!/bin/sh

if [ $(id -u) -ne 0 ]; then
	echo "run me as root"
	exit 1
fi

dest=''
if [ -e /usr/local/share/fonts/misc ]; then
	dest=/usr/local/share/fonts/misc
elif [ -e /usr/share/fonts/X11/misc ]; then
	dest=/usr/share/fonts/X11/misc
elif [ -e /usr/share/X11/fonts/misc ]; then
	dest=/usr/share/X11/fonts/misc
elif [ -e /usr/share/fonts/misc ]; then
	dest=/usr/share/fonts/misc
fi

if [ -z "$dest" ]; then
	echo "system bitmap fontpath not found"
	exit 1
else
	echo "fontpath: $dest"
fi

install -m 0644 -o root -g wheel *.pcf.gz $dest
if command -vp mkfontdir > /dev/null 2>&1; then
	mkfontdir $dest
	xset +fp $dest
fi
xset fp rehash
echo "running fc-cache; please wait..."
fc-cache -rf
echo "done"

